package wordle;

public class Letter {
    private int index;
    private String character;
    private boolean isCorrectPlacment;
    private boolean isValidCharacter;

    public Letter(int index, String character, boolean isCorrectPlacment, boolean isValidCharacter) {
        this.index = index;
        this.character = character;
        this.isCorrectPlacment = isCorrectPlacment;
        this.isValidCharacter = isValidCharacter;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getCharacter() {
        return character;
    }

    public void setCharacter(String character) {
        this.character = character;
    }

    public boolean isCorrectPlacment() {
        return isCorrectPlacment;
    }

    public void setCorrectPlacment(boolean correctPlacment) {
        isCorrectPlacment = correctPlacment;
    }

    public boolean isValidCharacter() {
        return isValidCharacter;
    }

    public void setValidCharacter(boolean validCharacter) {
        isValidCharacter = validCharacter;
    }
}
