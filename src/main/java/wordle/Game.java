package wordle;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;

public class Game {
    String dailyWord;
    List<List<Letter>> attempts;
    HashSet<String> validLetters;
    HashSet<String> invalidLetters;
    boolean isDone;
    CustomLogger logger = new CustomLogger();
    Game(String word) {
        dailyWord = word.toLowerCase(Locale.ROOT);
        attempts = new ArrayList<>();
        validLetters = new HashSet<>();
        invalidLetters = new HashSet<>();
        isDone = false;
    }

    public void updateAttempts(List<Letter> attempt) {
        this.attempts.add(attempt);
    }

    boolean isGameDone() {
        return isDone || attempts.size() == 5;
    }

    void showAttempts() {
        // CURRENT ATTEMPTS
        StringBuilder builder = new StringBuilder();
        builder.append("\n---CURRENT ATTEMPTS---\n");
        String invalidMsg = "Invalid Characters: "+logger.whiteText(String.join(", ", invalidLetters))+"\n\n";
        builder.append(invalidMsg);
        for(List<Letter> attempt: attempts) {
            for(Letter letter: attempt) {
                String character = letter.getCharacter();
                if(letter.isValidCharacter()) {
                    String colorMsg = letter.isCorrectPlacment() ? logger.greenText(character)
                            : logger.yellowText(character);
                    builder.append(colorMsg);
                }
                else {
                    builder.append(logger.whiteText(character));
                }
            }
            builder.append("\n");
        }

        logger.log(builder.toString(), true);

    }

    void validateWord(String attempt) {
        if (isCorrectWord(attempt)) {
            logger.success("Congrats you've guessed the correct word!!!");
            logger.log(logger.whiteText("The word of the day is: ")+logger.correctWord(attempt),true);
            isDone = true;
        } else {
            List<String> letters = Arrays.asList(dailyWord.split(""));
            List<String> attemptLetters = Arrays.asList(attempt.split(""));
            List<Letter> currentAttempt = new ArrayList<>();
            for (int i = 0; i < letters.size(); i++) {
                int index = letters.indexOf(attemptLetters.get(i));
                String character;
                Letter letter;

                if (index > -1) {
                    character = letters.get(index);
                    if (index == i) {
                        // correct placement
                        letter = new Letter(index, character, true, true);
                    }
                    else {
                        // correct letter wrong placement
                        letter = new Letter(index, character, false, true);
                    }
                    validLetters.add(character);
                }

                else {
                    character = attemptLetters.get(i);
                    // invalid character
                    letter = new Letter(i, character, false, false);
                    invalidLetters.add(character);
                }
                currentAttempt.add(letter);
            }
            this.updateAttempts(currentAttempt);
        }
    }

    boolean isCorrectWord(String attempt) {
        return dailyWord.equals(attempt);
    }
}
