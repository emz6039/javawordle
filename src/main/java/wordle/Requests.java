package wordle;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class Requests {

    String get(String url){
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder(URI.create(url))
                .header("accept", "application/json")
                .build();
        String msg = null;
        try{
            HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
            msg = response.body();
            System.out.println("Message response "+msg);
        }
        catch (IOException|InterruptedException error){
            System.out.println("Unable to process http request: "+error.getMessage());
        }
        return msg;
    }

    String post(String url, String payload){
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder(URI.create(url))
                .header("accept", "application/json")
                .header("Content-Type", "application/json")
                .POST(HttpRequest.BodyPublishers.ofString(payload))
                .build();
        String msg = null;
        try{
            HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
            System.out.println("Message response " + response.body());
            System.out.println("Message response " + response.statusCode());
        }
        catch (IOException|InterruptedException error){
            System.out.println("Unable to process http request: "+error.getMessage());
        }
        return msg;
    }
}
