package wordle;

public class CustomLogger {
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_BLACK = "\u001B[30m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_BLUE = "\u001B[34m";
    public static final String ANSI_PURPLE = "\u001B[35m";
    public static final String ANSI_CYAN = "\u001B[36m";
    public static final String ANSI_WHITE = "\u001B[37m";
    public static final String ANSI_BLACK_BACKGROUND = "\u001B[40m";
    public static final String ANSI_RED_BACKGROUND = "\u001B[41m";
    public static final String ANSI_GREEN_BACKGROUND = "\u001B[42m";
    public static final String ANSI_YELLOW_BACKGROUND = "\u001B[43m";
    public static final String ANSI_BLUE_BACKGROUND = "\u001B[44m";
    public static final String ANSI_PURPLE_BACKGROUND = "\u001B[45m";
    public static final String ANSI_CYAN_BACKGROUND = "\u001B[46m";
    public static final String ANSI_WHITE_BACKGROUND = "\u001B[47m";

    String blackText(String msg) { return ANSI_BLACK + msg; }

    String greenText(String msg) { return ANSI_GREEN + msg; }

    String redText(String msg) { return ANSI_RED + msg; }

    String yellowText(String msg) { return ANSI_YELLOW + msg; }

    String cyanText(String msg) { return ANSI_CYAN + msg; }

    String correctWord(String msg) { return ANSI_GREEN_BACKGROUND + ANSI_BLACK + msg; }

    String whiteText(String msg) { return ANSI_WHITE + msg; }

    void log(String msg, boolean nextLine) {
        System.out.print(msg + ANSI_RESET);
        if (nextLine) {
            System.out.println();
        }
    }

    void success(String msg) {
        log(greenText(msg), true);
    }

    void error(String msg) {
        log(redText(msg), true);
    }

    void info(String msg) {
        log(yellowText(msg), true);
    }

    void debug(String msg) {
        log(cyanText(msg), true);
    }

    void print(String msg) {
        System.out.print(msg);
    }

    void println(String msg) {
        System.out.println(msg);
    }
}
